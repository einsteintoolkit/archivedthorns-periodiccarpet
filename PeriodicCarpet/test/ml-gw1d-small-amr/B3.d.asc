# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:53:31-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small-amr.par"
#
# B3 d (B3)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	2 2 2	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	4 4 4	0	-1.05 -0.05 -0.05	0
0	0 0 0 0	6 6 6	0	-1 0 0	0
0	0 0 0 0	8 8 8	0	-0.95 0.05 0.05	0
0	0 0 0 0	10 10 10	0	-0.9 0.1 0.1	0
0	0 0 0 0	12 12 12	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 1 1 0	3 3 3	0	-1.075 -0.075 -0.075	0
0	0 1 1 0	4 4 4	0	-1.05 -0.05 -0.05	0
0	0 1 1 0	5 5 5	0	-1.025 -0.025 -0.025	0
0	0 1 1 0	6 6 6	0	-1 0 0	0
0	0 1 1 0	7 7 7	0	-0.975 0.025 0.025	0
0	0 1 1 0	8 8 8	0	-0.95 0.05 0.05	0
0	0 1 1 0	9 9 9	0	-0.925 0.075 0.075	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 1 1 0	3 3 3	0.00625	-1.075 -0.075 -0.075	3.29878478191269e-19
1	0 1 1 0	4 4 4	0.00625	-1.05 -0.05 -0.05	5.57399566317912e-20
1	0 1 1 0	5 5 5	0.00625	-1.025 -0.025 -0.025	-3.45918354133249e-20
1	0 1 1 0	6 6 6	0.00625	-1 0 0	2.35683043512206e-20
1	0 1 1 0	7 7 7	0.00625	-0.975 0.025 0.025	-1.01415440490908e-19
1	0 1 1 0	8 8 8	0.00625	-0.95 0.05 0.05	1.7865695577312e-19
1	0 1 1 0	9 9 9	0.00625	-0.925 0.075 0.075	-1.04179854681627e-19
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	-5.24068691822606e-20
2	0 0 0 0	2 2 2	0.0125	-1.1 -0.1 -0.1	1.02023206819906e-19
2	0 0 0 0	4 4 4	0.0125	-1.05 -0.05 -0.05	5.78098403140991e-21
2	0 0 0 0	6 6 6	0.0125	-1 0 0	9.30491643424686e-21
2	0 0 0 0	8 8 8	0.0125	-0.95 0.05 0.05	2.21449555333806e-19
2	0 0 0 0	10 10 10	0.0125	-0.9 0.1 0.1	1.13572786791256e-19
2	0 0 0 0	12 12 12	0.0125	-0.85 0.15 0.15	-4.83438518805465e-20
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 1 1 0	3 3 3	0.0125	-1.075 -0.075 -0.075	3.79726481884486e-19
2	0 1 1 0	4 4 4	0.0125	-1.05 -0.05 -0.05	5.78098403140991e-21
2	0 1 1 0	5 5 5	0.0125	-1.025 -0.025 -0.025	-7.81963639955345e-20
2	0 1 1 0	6 6 6	0.0125	-1 0 0	9.30491643424686e-21
2	0 1 1 0	7 7 7	0.0125	-0.975 0.025 0.025	-6.7058228028775e-20
2	0 1 1 0	8 8 8	0.0125	-0.95 0.05 0.05	2.21449555333806e-19
2	0 1 1 0	9 9 9	0.0125	-0.925 0.075 0.075	9.58000352652114e-20
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 1 1 0	3 3 3	0.01875	-1.075 -0.075 -0.075	4.21707985220599e-19
3	0 1 1 0	4 4 4	0.01875	-1.05 -0.05 -0.05	5.30543261400082e-20
3	0 1 1 0	5 5 5	0.01875	-1.025 -0.025 -0.025	-1.19823814223352e-19
3	0 1 1 0	6 6 6	0.01875	-1 0 0	-8.65216382381216e-20
3	0 1 1 0	7 7 7	0.01875	-0.975 0.025 0.025	-8.32303164045169e-20
3	0 1 1 0	8 8 8	0.01875	-0.95 0.05 0.05	2.0316726276145e-19
3	0 1 1 0	9 9 9	0.01875	-0.925 0.075 0.075	2.38720485495341e-19
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	-1.0111897641343e-19
4	0 0 0 0	2 2 2	0.025	-1.1 -0.1 -0.1	-4.83327326260174e-20
4	0 0 0 0	4 4 4	0.025	-1.05 -0.05 -0.05	2.69748450222078e-19
4	0 0 0 0	6 6 6	0.025	-1 0 0	-1.7353720899317e-19
4	0 0 0 0	8 8 8	0.025	-0.95 0.05 0.05	4.40734463447907e-19
4	0 0 0 0	10 10 10	0.025	-0.9 0.1 0.1	1.58742804741116e-19
4	0 0 0 0	12 12 12	0.025	-0.85 0.15 0.15	-2.17256082518519e-20
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 1 1 0	3 3 3	0.025	-1.075 -0.075 -0.075	3.76609796013348e-19
4	0 1 1 0	4 4 4	0.025	-1.05 -0.05 -0.05	2.69748450222078e-19
4	0 1 1 0	5 5 5	0.025	-1.025 -0.025 -0.025	-1.9690987802046e-19
4	0 1 1 0	6 6 6	0.025	-1 0 0	-1.7353720899317e-19
4	0 1 1 0	7 7 7	0.025	-0.975 0.025 0.025	-9.05754009636643e-20
4	0 1 1 0	8 8 8	0.025	-0.95 0.05 0.05	4.40734463447907e-19
4	0 1 1 0	9 9 9	0.025	-0.925 0.075 0.075	3.64287544438684e-19
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


