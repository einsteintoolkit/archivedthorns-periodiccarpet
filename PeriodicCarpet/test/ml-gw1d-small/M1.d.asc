# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:54:14-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# M1 d (M1)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	-3.93191937064774e-08
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	-5.38060041990057e-08
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	-6.29724781269977e-08
0	0 0 0 0	3 3 3	0	-1 0 0	-6.59448770845404e-08
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	-6.24627423968099e-08
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	-5.28959201041151e-08
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	-3.82066939128012e-08
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	2.14196768585917e-08
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	8.09948865442001e-09
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	-1.93783096427008e-09
1	0 0 0 0	3 3 3	0.0125	-1 0 0	-7.54959870465609e-09
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	-8.09500354552115e-09
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	-3.69201080645254e-09
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	5.1390207957136e-09
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	8.17468355507334e-08
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	7.02671154578088e-08
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	5.9914245815508e-08
2	0 0 0 0	3 3 3	0.025	-1 0 0	5.20919792709393e-08
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	4.78715388137554e-08
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	4.72453268512626e-08
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	5.02643296637628e-08
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	1.41373756302327e-07
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	1.32422824429893e-07
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	1.22345829755427e-07
3	0 0 0 0	3 3 3	0.0375	-1 0 0	1.12807012845467e-07
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	1.05268710974722e-07
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	9.98336880777065e-08
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	9.71146334755729e-08
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	2.00010432566816e-07
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	1.94280439057629e-07
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	1.85108686682686e-07
4	0 0 0 0	3 3 3	0.05	-1 0 0	1.74409006758504e-07
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	1.63911545763857e-07
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	1.53975002624776e-07
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	1.45626668774171e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


