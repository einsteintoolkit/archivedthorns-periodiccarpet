# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:54:14-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# At12 d (At12)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	0
0	0 0 0 0	3 3 3	0	-1 0 0	0
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	0
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	0
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	2.20501478951175e-16
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	-2.91539994810104e-16
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	-2.81822120105615e-17
1	0 0 0 0	3 3 3	0.0125	-1 0 0	3.14131764512916e-16
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	1.27730058443947e-16
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	-5.186351256831e-16
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	1.27658835274189e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	3.60915024521601e-16
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	-3.43346176294568e-16
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	-1.05952325191644e-16
2	0 0 0 0	3 3 3	0.025	-1 0 0	1.64824697381307e-16
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	-8.18915212751633e-17
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	-2.56760908489329e-16
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	-1.5525773160611e-18
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	3.03103762368095e-17
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	-3.99729268852604e-16
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	1.13601560769012e-16
3	0 0 0 0	3 3 3	0.0375	-1 0 0	1.13304754975585e-16
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	-3.4307179011497e-16
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	-3.89508621655379e-16
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	8.05873710492718e-17
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	-2.06337040766951e-16
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	-3.95999224268547e-16
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	-2.40502209571163e-16
4	0 0 0 0	3 3 3	0.05	-1 0 0	5.38390945165037e-17
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	-7.37974273288639e-16
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	-2.30480145591738e-16
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	-5.46063663229609e-18
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


