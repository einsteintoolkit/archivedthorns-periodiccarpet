# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:54:14-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# H d (H)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	-9.399916295034e-07
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	-5.88491685443094e-07
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	-2.31519295411732e-07
0	0 0 0 0	3 3 3	0	-1 0 0	1.20905856177523e-07
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	4.59871536585363e-07
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	7.77779908996412e-07
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	1.06843425754594e-06
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	-1.03397405136126e-06
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	-6.83044239132387e-07
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	-3.23681031610891e-07
1	0 0 0 0	3 3 3	0.0125	-1 0 0	3.39498345986322e-08
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	3.79874546578472e-07
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	7.07000693950119e-07
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	1.00647129033196e-06
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	-1.12721433613177e-06
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	-7.77780077369745e-07
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	-4.16744334779883e-07
2	0 0 0 0	3 3 3	0.025	-1 0 0	-5.45806532060368e-08
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	2.97963015022727e-07
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	6.33807590170266e-07
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	9.4219647584036e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	-1.21946735485239e-06
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	-8.7249825193159e-07
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	-5.10530837602492e-07
3	0 0 0 0	3 3 3	0.0375	-1 0 0	-1.44551360331231e-07
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	2.1426766656652e-07
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	5.58211070605018e-07
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	8.75682181559693e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	-1.31049860346844e-06
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	-9.66981351401744e-07
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	-6.04865161407725e-07
4	0 0 0 0	3 3 3	0.05	-1 0 0	-2.35802572357874e-07
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	1.28892929119764e-07
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	4.80283065270503e-07
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	8.06951165436879e-07
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


