# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:54:14-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# B2 d (B2)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	0
0	0 0 0 0	3 3 3	0	-1 0 0	0
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	0
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	0
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	-1.31533150359623e-19
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	4.9650942969296e-20
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	2.03375315017155e-19
1	0 0 0 0	3 3 3	0.0125	-1 0 0	7.8952836219154e-20
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	7.30903185275006e-20
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	2.03024817543928e-19
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	2.42493050634792e-20
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	-3.48218254151825e-19
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	1.87917417524183e-19
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	2.04800214855197e-19
2	0 0 0 0	3 3 3	0.025	-1 0 0	-1.64404432752643e-19
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	1.42279056038338e-19
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	2.51896413684069e-19
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	-8.22668179244859e-20
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	-3.78326557989112e-19
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	2.91758470986648e-19
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	1.53159080052607e-19
3	0 0 0 0	3 3 3	0.0375	-1 0 0	-2.29068510543915e-19
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	3.0476029177925e-19
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	2.4449824461713e-19
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	-1.22613382466117e-19
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	-3.54374403368305e-19
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	4.67300432559546e-19
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	5.80105144397901e-21
4	0 0 0 0	3 3 3	0.05	-1 0 0	-1.73827268374467e-19
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	4.8114342877422e-19
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	2.97189248402471e-19
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	-2.44747514283103e-19
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


