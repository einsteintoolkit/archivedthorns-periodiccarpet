# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:54:14-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# kxx d (kxx)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0.0139641637748424
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	0.0149160963702215
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	0.015502441838192
0	0 0 0 0	3 3 3	0	-1 0 0	0.0157079632634449
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	0.0155267119935513
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	0.0149622609235073
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	0.0140277038871666
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	0.0136716866884761
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	0.0147115893300781
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	0.0153911168817682
1	0 0 0 0	3 3 3	0.0125	-1 0 0	0.0156927728754317
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	0.0156082557866458
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	0.0151387488147239
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	0.0142949787693352
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	0.0133582830472015
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	0.014484515660543
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	0.0152561293084447
2	0 0 0 0	3 3 3	0.025	-1 0 0	0.0156533996413471
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	0.0156656904435178
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	0.0152917989509824
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	0.0145400735466849
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	0.0130244429943882
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	0.0142352358902699
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	0.0150977001498971
3	0 0 0 0	3 3 3	0.0375	-1 0 0	0.0155899183978452
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	0.0156989413881622
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	0.0154211878014608
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	0.0147626200273373
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	0.0126706871908783
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	0.0139641439956158
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	0.0149160860698749
4	0 0 0 0	3 3 3	0.05	-1 0 0	0.0155024409725649
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	0.0157079715015705
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	0.0155267288208674
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	0.0149622855749785
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


