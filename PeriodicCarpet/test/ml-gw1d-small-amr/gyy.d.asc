# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:53:31-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small-amr.par"
#
# gyy d (gyy)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	1
0	0 0 0 0	2 2 2	0	-1.1 -0.1 -0.1	1
0	0 0 0 0	4 4 4	0	-1.05 -0.05 -0.05	1
0	0 0 0 0	6 6 6	0	-1 0 0	1
0	0 0 0 0	8 8 8	0	-0.95 0.05 0.05	1
0	0 0 0 0	10 10 10	0	-0.9 0.1 0.1	1
0	0 0 0 0	12 12 12	0	-0.85 0.15 0.15	1
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 1 1 0	3 3 3	0	-1.075 -0.075 -0.075	1
0	0 1 1 0	4 4 4	0	-1.05 -0.05 -0.05	1
0	0 1 1 0	5 5 5	0	-1.025 -0.025 -0.025	1
0	0 1 1 0	6 6 6	0	-1 0 0	1
0	0 1 1 0	7 7 7	0	-0.975 0.025 0.025	1
0	0 1 1 0	8 8 8	0	-0.95 0.05 0.05	1
0	0 1 1 0	9 9 9	0	-0.925 0.075 0.075	1
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 1 1 0	3 3 3	0.00625	-1.075 -0.075 -0.075	0.999999999999673
1	0 1 1 0	4 4 4	0.00625	-1.05 -0.05 -0.05	0.999999999999938
1	0 1 1 0	5 5 5	0.00625	-1.025 -0.025 -0.025	0.999999999999156
1	0 1 1 0	6 6 6	0.00625	-1 0 0	1.00000000002605
1	0 1 1 0	7 7 7	0.00625	-0.975 0.025 0.025	0.999999999494709
1	0 1 1 0	8 8 8	0.00625	-0.95 0.05 0.05	1.00000000223197
1	0 1 1 0	9 9 9	0.00625	-0.925 0.075 0.075	1.00000005777174
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	0.9999999999712
2	0 0 0 0	2 2 2	0.0125	-1.1 -0.1 -0.1	0.999999999998393
2	0 0 0 0	4 4 4	0.0125	-1.05 -0.05 -0.05	0.999999999977832
2	0 0 0 0	6 6 6	0.0125	-1 0 0	1.00000000019759
2	0 0 0 0	8 8 8	0.0125	-0.95 0.05 0.05	1.00000008187252
2	0 0 0 0	10 10 10	0.0125	-0.9 0.1 0.1	0.999995860503359
2	0 0 0 0	12 12 12	0.0125	-0.85 0.15 0.15	0.99999999982801
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 1 1 0	3 3 3	0.0125	-1.075 -0.075 -0.075	0.999999999999032
2	0 1 1 0	4 4 4	0.0125	-1.05 -0.05 -0.05	0.999999999977832
2	0 1 1 0	5 5 5	0.0125	-1.025 -0.025 -0.025	1.00000000017701
2	0 1 1 0	6 6 6	0.0125	-1 0 0	1.00000000019759
2	0 1 1 0	7 7 7	0.0125	-0.975 0.025 0.025	0.999999985874284
2	0 1 1 0	8 8 8	0.0125	-0.95 0.05 0.05	1.00000008187252
2	0 1 1 0	9 9 9	0.0125	-0.925 0.075 0.075	1.00000005197827
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 1 1 0	3 3 3	0.01875	-1.075 -0.075 -0.075	0.999999999985078
3	0 1 1 0	4 4 4	0.01875	-1.05 -0.05 -0.05	0.999999999901113
3	0 1 1 0	5 5 5	0.01875	-1.025 -0.025 -0.025	1.00000000143813
3	0 1 1 0	6 6 6	0.01875	-1 0 0	0.999999996258915
3	0 1 1 0	7 7 7	0.01875	-0.975 0.025 0.025	0.999999951340939
3	0 1 1 0	8 8 8	0.01875	-0.95 0.05 0.05	1.00000035403668
3	0 1 1 0	9 9 9	0.01875	-0.925 0.075 0.075	0.999999448735529
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	1.00000000008652
4	0 0 0 0	2 2 2	0.025	-1.1 -0.1 -0.1	1.00000000001018
4	0 0 0 0	4 4 4	0.025	-1.05 -0.05 -0.05	0.999999999981425
4	0 0 0 0	6 6 6	0.025	-1 0 0	0.999999976744342
4	0 0 0 0	8 8 8	0.025	-0.95 0.05 0.05	1.00000085804339
4	0 0 0 0	10 10 10	0.025	-0.9 0.1 0.1	0.999985388786678
4	0 0 0 0	12 12 12	0.025	-0.85 0.15 0.15	0.999999222559264
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 1 1 0	3 3 3	0.025	-1.075 -0.075 -0.075	0.999999999903074
4	0 1 1 0	4 4 4	0.025	-1.05 -0.05 -0.05	0.999999999981425
4	0 1 1 0	5 5 5	0.025	-1.025 -0.025 -0.025	1.00000000472588
4	0 1 1 0	6 6 6	0.025	-1 0 0	0.999999976744342
4	0 1 1 0	7 7 7	0.025	-0.975 0.025 0.025	0.999999922406215
4	0 1 1 0	8 8 8	0.025	-0.95 0.05 0.05	1.00000085804339
4	0 1 1 0	9 9 9	0.025	-0.925 0.075 0.075	0.999997661608645
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


