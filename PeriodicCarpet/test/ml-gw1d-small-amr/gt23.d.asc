# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:53:31-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small-amr.par"
#
# gt23 d (gt23)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	2 2 2	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	4 4 4	0	-1.05 -0.05 -0.05	0
0	0 0 0 0	6 6 6	0	-1 0 0	0
0	0 0 0 0	8 8 8	0	-0.95 0.05 0.05	0
0	0 0 0 0	10 10 10	0	-0.9 0.1 0.1	0
0	0 0 0 0	12 12 12	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 1 1 0	3 3 3	0	-1.075 -0.075 -0.075	0
0	0 1 1 0	4 4 4	0	-1.05 -0.05 -0.05	0
0	0 1 1 0	5 5 5	0	-1.025 -0.025 -0.025	0
0	0 1 1 0	6 6 6	0	-1 0 0	0
0	0 1 1 0	7 7 7	0	-0.975 0.025 0.025	0
0	0 1 1 0	8 8 8	0	-0.95 0.05 0.05	0
0	0 1 1 0	9 9 9	0	-0.925 0.075 0.075	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 1 1 0	3 3 3	0.00625	-1.075 -0.075 -0.075	1.28119020598147e-18
1	0 1 1 0	4 4 4	0.00625	-1.05 -0.05 -0.05	-6.22719393627429e-19
1	0 1 1 0	5 5 5	0.00625	-1.025 -0.025 -0.025	-1.33499800663224e-18
1	0 1 1 0	6 6 6	0.00625	-1 0 0	4.13004587326031e-19
1	0 1 1 0	7 7 7	0.00625	-0.975 0.025 0.025	-2.61965119474078e-18
1	0 1 1 0	8 8 8	0.00625	-0.95 0.05 0.05	-2.63326690969887e-18
1	0 1 1 0	9 9 9	0.00625	-0.925 0.075 0.075	-1.90921970450966e-18
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	1.88588718501314e-18
2	0 0 0 0	2 2 2	0.0125	-1.1 -0.1 -0.1	1.27640786595963e-17
2	0 0 0 0	4 4 4	0.0125	-1.05 -0.05 -0.05	2.78622699642162e-18
2	0 0 0 0	6 6 6	0.0125	-1 0 0	-2.8021674995447e-18
2	0 0 0 0	8 8 8	0.0125	-0.95 0.05 0.05	-6.0724481116266e-18
2	0 0 0 0	10 10 10	0.0125	-0.9 0.1 0.1	-9.15921755739813e-18
2	0 0 0 0	12 12 12	0.0125	-0.85 0.15 0.15	2.21021111586203e-18
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 1 1 0	3 3 3	0.0125	-1.075 -0.075 -0.075	-5.36403992724914e-19
2	0 1 1 0	4 4 4	0.0125	-1.05 -0.05 -0.05	2.78622699642162e-18
2	0 1 1 0	5 5 5	0.0125	-1.025 -0.025 -0.025	1.72412814569041e-18
2	0 1 1 0	6 6 6	0.0125	-1 0 0	-2.8021674995447e-18
2	0 1 1 0	7 7 7	0.0125	-0.975 0.025 0.025	-6.77122082510729e-18
2	0 1 1 0	8 8 8	0.0125	-0.95 0.05 0.05	-6.0724481116266e-18
2	0 1 1 0	9 9 9	0.0125	-0.925 0.075 0.075	-7.63795456606035e-18
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 1 1 0	3 3 3	0.01875	-1.075 -0.075 -0.075	4.73063560666854e-18
3	0 1 1 0	4 4 4	0.01875	-1.05 -0.05 -0.05	8.20721052663384e-18
3	0 1 1 0	5 5 5	0.01875	-1.025 -0.025 -0.025	7.06625429032965e-18
3	0 1 1 0	6 6 6	0.01875	-1 0 0	-8.29664998049416e-18
3	0 1 1 0	7 7 7	0.01875	-0.975 0.025 0.025	-1.26889869049303e-17
3	0 1 1 0	8 8 8	0.01875	-0.95 0.05 0.05	-8.95779047073184e-18
3	0 1 1 0	9 9 9	0.01875	-0.925 0.075 0.075	-1.43954643750639e-17
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	5.28095507456533e-19
4	0 0 0 0	2 2 2	0.025	-1.1 -0.1 -0.1	1.31427493158773e-17
4	0 0 0 0	4 4 4	0.025	-1.05 -0.05 -0.05	1.38486387871654e-17
4	0 0 0 0	6 6 6	0.025	-1 0 0	-2.26159069715462e-17
4	0 0 0 0	8 8 8	0.025	-0.95 0.05 0.05	-1.27009764535103e-17
4	0 0 0 0	10 10 10	0.025	-0.9 0.1 0.1	-1.14772929249023e-17
4	0 0 0 0	12 12 12	0.025	-0.85 0.15 0.15	2.23470977566073e-18
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 1 1 0	3 3 3	0.025	-1.075 -0.075 -0.075	1.07567154423459e-17
4	0 1 1 0	4 4 4	0.025	-1.05 -0.05 -0.05	1.38486387871654e-17
4	0 1 1 0	5 5 5	0.025	-1.025 -0.025 -0.025	1.29212037953172e-17
4	0 1 1 0	6 6 6	0.025	-1 0 0	-2.26159069715462e-17
4	0 1 1 0	7 7 7	0.025	-0.975 0.025 0.025	-2.05006712380032e-17
4	0 1 1 0	8 8 8	0.025	-0.95 0.05 0.05	-1.27009764535103e-17
4	0 1 1 0	9 9 9	0.025	-0.925 0.075 0.075	-2.15799359438559e-17
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


