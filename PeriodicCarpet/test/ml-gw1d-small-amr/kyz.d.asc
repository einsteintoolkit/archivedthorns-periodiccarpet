# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:53:31-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small-amr.par"
#
# kyz d (kyz)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	2 2 2	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	4 4 4	0	-1.05 -0.05 -0.05	0
0	0 0 0 0	6 6 6	0	-1 0 0	0
0	0 0 0 0	8 8 8	0	-0.95 0.05 0.05	0
0	0 0 0 0	10 10 10	0	-0.9 0.1 0.1	0
0	0 0 0 0	12 12 12	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 1 1 0	3 3 3	0	-1.075 -0.075 -0.075	0
0	0 1 1 0	4 4 4	0	-1.05 -0.05 -0.05	0
0	0 1 1 0	5 5 5	0	-1.025 -0.025 -0.025	0
0	0 1 1 0	6 6 6	0	-1 0 0	0
0	0 1 1 0	7 7 7	0	-0.975 0.025 0.025	0
0	0 1 1 0	8 8 8	0	-0.95 0.05 0.05	0
0	0 1 1 0	9 9 9	0	-0.925 0.075 0.075	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.00625
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 1 1 0	3 3 3	0.00625	-1.075 -0.075 -0.075	1.28755162746908e-16
1	0 1 1 0	4 4 4	0.00625	-1.05 -0.05 -0.05	1.55785835455961e-16
1	0 1 1 0	5 5 5	0.00625	-1.025 -0.025 -0.025	-9.8730421518356e-17
1	0 1 1 0	6 6 6	0.00625	-1 0 0	3.8783448411255e-17
1	0 1 1 0	7 7 7	0.00625	-0.975 0.025 0.025	4.32888448663807e-16
1	0 1 1 0	8 8 8	0.00625	-0.95 0.05 0.05	5.42104546054583e-16
1	0 1 1 0	9 9 9	0.00625	-0.925 0.075 0.075	8.2732248972606e-17
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	-9.24118411624505e-17
2	0 0 0 0	2 2 2	0.0125	-1.1 -0.1 -0.1	-1.85816942484028e-16
2	0 0 0 0	4 4 4	0.0125	-1.05 -0.05 -0.05	-5.43505100557068e-16
2	0 0 0 0	6 6 6	0.0125	-1 0 0	3.70857819815495e-16
2	0 0 0 0	8 8 8	0.0125	-0.95 0.05 0.05	1.64272702385663e-16
2	0 0 0 0	10 10 10	0.0125	-0.9 0.1 0.1	6.9748364780329e-16
2	0 0 0 0	12 12 12	0.0125	-0.85 0.15 0.15	-1.47865831733745e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.0125
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 1 1 0	3 3 3	0.0125	-1.075 -0.075 -0.075	1.85416929129734e-16
2	0 1 1 0	4 4 4	0.0125	-1.05 -0.05 -0.05	-5.43505100557068e-16
2	0 1 1 0	5 5 5	0.0125	-1.025 -0.025 -0.025	-3.4819966108952e-16
2	0 1 1 0	6 6 6	0.0125	-1 0 0	3.70857819815495e-16
2	0 1 1 0	7 7 7	0.0125	-0.975 0.025 0.025	4.80774972341608e-16
2	0 1 1 0	8 8 8	0.0125	-0.95 0.05 0.05	1.64272702385663e-16
2	0 1 1 0	9 9 9	0.0125	-0.925 0.075 0.075	4.09669477669762e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.01875
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 1 1 0	3 3 3	0.01875	-1.075 -0.075 -0.075	-5.3754105821976e-16
3	0 1 1 0	4 4 4	0.01875	-1.05 -0.05 -0.05	-4.69703026725882e-16
3	0 1 1 0	5 5 5	0.01875	-1.025 -0.025 -0.025	-3.85365505445563e-16
3	0 1 1 0	6 6 6	0.01875	-1 0 0	8.96890486882547e-16
3	0 1 1 0	7 7 7	0.01875	-0.975 0.025 0.025	4.5765600053337e-16
3	0 1 1 0	8 8 8	0.01875	-0.95 0.05 0.05	3.71266185550515e-16
3	0 1 1 0	9 9 9	0.01875	-0.925 0.075 0.075	5.26696406564501e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	-4.8838229051515e-16
4	0 0 0 0	2 2 2	0.025	-1.1 -0.1 -0.1	5.11399516684778e-16
4	0 0 0 0	4 4 4	0.025	-1.05 -0.05 -0.05	-4.07502662804755e-16
4	0 0 0 0	6 6 6	0.025	-1 0 0	1.32261187520412e-15
4	0 0 0 0	8 8 8	0.025	-0.95 0.05 0.05	3.99347310593164e-16
4	0 0 0 0	10 10 10	0.025	-0.9 0.1 0.1	-3.72774967982631e-16
4	0 0 0 0	12 12 12	0.025	-0.85 0.15 0.15	1.40835047105845e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.025
# time level 0
# refinement level 1   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 1 1 0	3 3 3	0.025	-1.075 -0.075 -0.075	-5.62297880875417e-16
4	0 1 1 0	4 4 4	0.025	-1.05 -0.05 -0.05	-4.07502662804755e-16
4	0 1 1 0	5 5 5	0.025	-1.025 -0.025 -0.025	-6.78920757686959e-16
4	0 1 1 0	6 6 6	0.025	-1 0 0	1.32261187520412e-15
4	0 1 1 0	7 7 7	0.025	-0.975 0.025 0.025	8.07247934799304e-16
4	0 1 1 0	8 8 8	0.025	-0.95 0.05 0.05	3.99347310593164e-16
4	0 1 1 0	9 9 9	0.025	-0.925 0.075 0.075	3.60244045387409e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


