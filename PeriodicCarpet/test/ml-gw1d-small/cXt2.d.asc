# 1D ASCII output created by CarpetIOASCII
# created on Redshift.local by eschnett on Mar 03 2013 at 17:54:14-0500
# parameter filename: "/Users/eschnett/Cvanilla/arrangements/LSUThorns/PeriodicCarpet/test/ml-gw1d-small.par"
#
# cXt2 d (cXt2)
#
# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
0	0 0 0 0	0 0 0	0	-1.15 -0.15 -0.15	0
0	0 0 0 0	1 1 1	0	-1.1 -0.1 -0.1	0
0	0 0 0 0	2 2 2	0	-1.05 -0.05 -0.05	4.93038065763132e-32
0	0 0 0 0	3 3 3	0	-1 0 0	0
0	0 0 0 0	4 4 4	0	-0.95 0.05 0.05	0
0	0 0 0 0	5 5 5	0	-0.9 0.1 0.1	0
0	0 0 0 0	6 6 6	0	-0.85 0.15 0.15	0
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 0   time 0
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
1	0 0 0 0	0 0 0	0.0125	-1.15 -0.15 -0.15	3.64748534176369e-16
1	0 0 0 0	1 1 1	0.0125	-1.1 -0.1 -0.1	3.41572857268203e-16
1	0 0 0 0	2 2 2	0.0125	-1.05 -0.05 -0.05	1.77028944030746e-16
1	0 0 0 0	3 3 3	0.0125	-1 0 0	1.38058640857025e-16
1	0 0 0 0	4 4 4	0.0125	-0.95 0.05 0.05	-4.45504242838541e-16
1	0 0 0 0	5 5 5	0.0125	-0.9 0.1 0.1	-3.56179618038345e-16
1	0 0 0 0	6 6 6	0.0125	-0.85 0.15 0.15	-2.46802297144349e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 1   time 0.0125
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
2	0 0 0 0	0 0 0	0.025	-1.15 -0.15 -0.15	3.79147457590414e-16
2	0 0 0 0	1 1 1	0.025	-1.1 -0.1 -0.1	6.62559877682854e-17
2	0 0 0 0	2 2 2	0.025	-1.05 -0.05 -0.05	3.53444550806925e-16
2	0 0 0 0	3 3 3	0.025	-1 0 0	-6.74738582147518e-17
2	0 0 0 0	4 4 4	0.025	-0.95 0.05 0.05	3.43935635317059e-16
2	0 0 0 0	5 5 5	0.025	-0.9 0.1 0.1	1.95994285306353e-16
2	0 0 0 0	6 6 6	0.025	-0.85 0.15 0.15	1.36033140777295e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 2   time 0.025
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
3	0 0 0 0	0 0 0	0.0375	-1.15 -0.15 -0.15	4.64069309270445e-16
3	0 0 0 0	1 1 1	0.0375	-1.1 -0.1 -0.1	2.94756627562941e-16
3	0 0 0 0	2 2 2	0.0375	-1.05 -0.05 -0.05	-1.77255676471636e-16
3	0 0 0 0	3 3 3	0.0375	-1 0 0	-1.45697010780443e-16
3	0 0 0 0	4 4 4	0.0375	-0.95 0.05 0.05	4.94995086164775e-16
3	0 0 0 0	5 5 5	0.0375	-0.9 0.1 0.1	-8.2051853025056e-17
3	0 0 0 0	6 6 6	0.0375	-0.85 0.15 0.15	-3.79125563208675e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 3   time 0.0375
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 0
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
4	0 0 0 0	0 0 0	0.05	-1.15 -0.15 -0.15	3.9047297355431e-16
4	0 0 0 0	1 1 1	0.05	-1.1 -0.1 -0.1	6.38849502629737e-16
4	0 0 0 0	2 2 2	0.05	-1.05 -0.05 -0.05	-2.39125395550728e-18
4	0 0 0 0	3 3 3	0.05	-1 0 0	2.0889006842447e-16
4	0 0 0 0	4 4 4	0.05	-0.95 0.05 0.05	9.63414859987945e-17
4	0 0 0 0	5 5 5	0.05	-0.9 0.1 0.1	-2.58353008297946e-16
4	0 0 0 0	6 6 6	0.05	-0.85 0.15 0.15	-3.55090842643939e-16
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

# iteration 4   time 0.05
# time level 0
# refinement level 0   multigrid level 0   map 0   component 1
# column format: 1:it	2:tl	3:rl 4:c 5:ml	6:ix 7:iy 8:iz	9:time	10:x 11:y 12:z	13:data
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#


